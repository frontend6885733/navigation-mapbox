import mapboxgl from "mapbox-gl";
import "./map.css";
import { useRef, useEffect, useState } from "react";
//components
import SearchResult from "./SearchResult";
//custom hooks
import useFetchDirections from "./hooks/useFetchDirections";
import useGeocode from "./hooks/useGeocode";
//util funcs
import { layer } from "./utelities/layer";
import { createMarker } from "./utelities/createMarker";
import UserLocation from "./components/UserLocation";

mapboxgl.accessToken =
  "pk.eyJ1IjoibW9zYTU0NDUiLCJhIjoiY2p3a3ppcjY1MHA1ZTQ0cDk5MDU1eXNxOCJ9.psGVr3_kzZWMk--4Ojbhdw";

function Map() {
  const [originName, setOriginName] = useState(null);
  const [originMarkerLoc, setOriginMarkerLoc] = useState([]);
  const [destMarkerLoc, setDestMarkerLoc] = useState([]);
  const [destinationName, setDestinationName] = useState(null);
  const [originFocus, setOriginFocus] = useState(true);
  const [originMarkerRef, setOriginMarkerRef] = useState(null);
  const [destinationMarkerRef, setDestinationMarkerRef] = useState(null);
  const [currentLoc, setCurrentLoc] = useState({ lat: 59.578749, lng: 36.313287 });
  const mapContainer = useRef(null);
  const map = useRef(null);
  //custom hooks
  const route = useFetchDirections(originMarkerLoc, destMarkerLoc);
  const originResults = useGeocode(originName);
  const destinationResults = useGeocode(destinationName);

  const handleMarker = (markerRef, markerLoc, color, setMarkerRef) => {
    if (markerRef) markerRef.remove();
    if (markerLoc.length !== 0) {
      const newMarker = createMarker(markerLoc, map.current, color);
      newMarker.on("dragend", () => {
        const newMarkerLoc = newMarker.getLngLat();
        if (markerLoc === originMarkerLoc) {
          setOriginMarkerLoc([newMarkerLoc.lng, newMarkerLoc.lat]);
        } else {
          setDestMarkerLoc([newMarkerLoc.lng, newMarkerLoc.lat]);
        }
      });
      setMarkerRef(newMarker);
    }
  };

  useEffect(() => {
    if (!map.current) {
      map.current = new mapboxgl.Map({
        container: "map",
        style: "mapbox://styles/mapbox/streets-v12",
        center: [currentLoc.lat, currentLoc.lng],
        zoom: 10,
      });
    }

    console.log(originMarkerRef);

    handleMarker(originMarkerRef, originMarkerLoc, "#6f6fec", setOriginMarkerRef);
    handleMarker(destinationMarkerRef, destMarkerLoc, "#f05e60", setDestinationMarkerRef);

    //ADDING ROUTE LAYER BETWEEN MARKERS
    if (route.length != 0) {
      const geojson = {
        type: "Feature",
        properties: {},
        geometry: {
          type: "LineString",
          coordinates: route,
        },
      };
      const sourceId = "LineString";
      const existingSource = map.current.getSource(sourceId);

      if (route.length !== 0) {
        if (!existingSource) {
          map.current.addSource(sourceId, {
            type: "geojson",
            data: geojson,
          });
        } else {
          map.current.getSource(sourceId).setData(geojson);
        }

        const existingLayer = map.current.getLayer("LineString");
        if (!existingLayer) {
          map.current.addLayer(layer);
        }
      }
      // Fit the map to the bounds of the route
      const bounds = geojson.geometry.coordinates.reduce(
        (bounds, coord) => bounds.extend(coord),
        new mapboxgl.LngLatBounds(
          geojson.geometry.coordinates[0],
          geojson.geometry.coordinates[0]
        )
      );
      map.current.fitBounds(bounds, {
        padding: { top: 50, bottom: 50, left: 500, right: 500 },
      });
    }

    // map.current.on("click", handleClick);
    //THIS WORKS-------------------------------------------------------
    map.current?.on("click", (e) => {
      console.log("ran", originFocus);
      const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
      console.log(e);
      setOriginMarkerLoc(clickedMarkerLoc);
    });
    map.current?.on("contextmenu", (e) => {
      const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
      setDestMarkerLoc(clickedMarkerLoc);
    });
  }, [originMarkerLoc, destMarkerLoc, route, currentLoc]);

  return (
    <div>
      <div className="search-box">
        <div className="main-search-field">
          <h2 className="search-box-title">Get Directions</h2>
          <div className="input-wrapper">
            <input
              type="text"
              onChange={(e) =>
                setTimeout(() => {
                  setOriginName(e.target.value);
                }, 500)
              }
              onFocus={() => {
                setOriginFocus(true);
              }}
              placeholder="Choose origin"
            />
          </div>
          <div className="input-wrapper">
            <input
              type="text"
              onChange={(e) =>
                setTimeout(() => {
                  setDestinationName(e.target.value);
                }, 500)
              }
              onFocus={() => {
                setOriginFocus(false);
              }}
              placeholder="Choose destination"
            />
          </div>
        </div>
        <UserLocation
          setOriginMarkerLoc={setOriginMarkerLoc}
          setCurrentLoc={setCurrentLoc}
        />
        <div className="results">
          {originFocus
            ? originResults.map((loc) => (
                <>
                  <SearchResult
                    key={loc.id}
                    location={loc}
                    setMarkerLoc={setOriginMarkerLoc}
                  />
                  <hr className="result-line" />
                </>
              ))
            : destinationResults.map((loc) => (
                <>
                  <SearchResult
                    key={loc.id}
                    location={loc}
                    setMarkerLoc={setDestMarkerLoc}
                  />
                  <hr className="result-line" />
                </>
              ))}
        </div>
      </div>

      <div ref={mapContainer} className="map-container" id="map" />
    </div>
  );
}

export default Map;

// console.log(originFocus);
// const handleClick = (e) => {
//   if (originFocus) {
//     console.log("ran", originFocus);
//     const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
//     setOriginMarkerLoc(clickedMarkerLoc);
//     map.current.off("click", handleClick);
//   } else {
//     const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
//     setDestMarkerLoc(clickedMarkerLoc);
//     map.current.off("click", handleClick);
//   }
// };
// map.current?.on("click", (e) => {
//   if (originFocus) {
//     console.log("ran", originFocus);
//     const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
//     setOriginMarkerLoc(clickedMarkerLoc);
//     setOriginFocus(false);
//   } else {
//     const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
//     setDestMarkerLoc(clickedMarkerLoc);
//   }
// });
//--------------------------------------------------------------

// console.log(originFocus);
// map.current?.on("click", (e) => {
//   setOriginFocus((prevFocus) => {
//     if (prevFocus) {
//       console.log("ran", prevFocus);
//       const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
//       setOriginMarkerLoc(clickedMarkerLoc);
//       return false; // Update originFocus to false
//     } else {
//       const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
//       setDestMarkerLoc(clickedMarkerLoc);
//       return true;
//     }
//   });
// });
//--------------------------------------------------------------
// console.log(originFocus);
// map.current?.on("click", () => {
//   const isCurrentOriginFocus = originFocus;
//   if (isCurrentOriginFocus) {
//     console.log("ran", isCurrentOriginFocus);
//   }
// });
// console.log(originFocus);
// console.log(originFocus);
// map.current?.on("click", () => {
//   if (originFocus) {
//     console.log("ran", originFocus);
//   }
// });
