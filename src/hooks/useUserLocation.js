import { useState, useEffect } from "react";

const useUserLocation = (clicked) => {
  const [lat, setLat] = useState([]);
  const [lng, setLng] = useState([]);

  useEffect(() => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setLat(position.coords.latitude);
          setLng(position.coords.longitude);
        },
        (error) => {
          console.error("Error getting user's location:", error);
        }
      );
    } else {
      console.log("Geolocation is not available in this browser.");
    }
  }, [clicked]);
  return { lat, lng };
};
export default useUserLocation;
