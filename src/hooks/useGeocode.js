import { useEffect, useState } from "react";
import axios from "axios";
import mapboxgl from "mapbox-gl";
function useGeocoding(query) {
  const [results, setResults] = useState([]);

  useEffect(() => {
    if (query) {
      axios
        .get(
          `https://api.mapbox.com/geocoding/v5/mapbox.places/${query}.json?access_token=${mapboxgl.accessToken}`
        )
        .then((resp) => setResults(resp.data.features))
        .catch((error) => console.error("Error fetching geocoding:", error));
    }
  }, [query]);

  return results;
}

export default useGeocoding;
