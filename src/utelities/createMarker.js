import mapboxgl from "mapbox-gl";

export function createMarker(position, map, color) {
  const marker = new mapboxgl.Marker({
    color: color,
    draggable: true,
  })
    .setLngLat(position)
    .addTo(map);
  return marker;
}
