import { useState } from "react";
import useUserLocation from "../hooks/useUserLocation";
import markSvg from "../assets/gps.svg";
import "./userLocation.css";
function UserLocation({ setOriginMarkerLoc, setCurrentLoc }) {
  const [clicked, setClicked] = useState(false);
  const { lat, lng } = useUserLocation(clicked);

  const onClickLocation = () => {
    setClicked(true);
    setCurrentLoc({ lat: lat, lng: lng });
    setOriginMarkerLoc([lat, lng]);
  };
  return (
    <div className="your-location" onClick={onClickLocation}>
      <div>
        <img src={markSvg} alt="gps" />
      </div>
      Your location
    </div>
  );
}

export default UserLocation;
