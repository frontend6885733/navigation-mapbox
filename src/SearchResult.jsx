/* eslint-disable react/prop-types */
import "./searchResult.css";
function SearchResult({ location, setMarkerLoc }) {
  return (
    <div className="location-card">
      <span onClick={() => setMarkerLoc(location.center)}>{location.place_name}</span>
    </div>
  );
}

export default SearchResult;
